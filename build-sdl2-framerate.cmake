# get SDL from the correct place
if (SDL2_FOUND)
  include_directories(${SDL2_INCLUDE_DIR})
else()
  include_directories(${FUSION_ROOT}/include/SDL2)
endif()

# Add as a static library
add_library(SDL2_framerate STATIC ${CMAKE_SOURCE_DIR}/src/SDL2_framerate/SDL2_framerate.c)
target_compile_options(SDL2_framerate PRIVATE -O2)
set(SDL2_FRAMERATE_HEADER ${CMAKE_SOURCE_DIR}/src/SDL2_framerate/SDL2_framerate.h)
install(TARGETS SDL2_framerate ARCHIVE DESTINATION lib)
add_dependencies(SDL2_framerate sdl2)

# copy on build, thats what we want
add_custom_command(TARGET SDL2_framerate POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E make_directory ${FUSION_ROOT}/include/SDL2_framerate/
                   COMMAND ${CMAKE_COMMAND} -E make_directory ${FUSION_ROOT}/lib/
                   COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:SDL2_framerate> ${FUSION_ROOT}/lib
                   COMMAND ${CMAKE_COMMAND} -E copy ${SDL2_FRAMERATE_HEADER} ${FUSION_ROOT}/include/SDL2_framerate/
)


