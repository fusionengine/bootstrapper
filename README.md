# Bootstrapper

Set of CMake scripts based on [G2CK] that can check if you have all dependencies necessary to build the engine or download them if you don't.

It assumes you have a **build environment** available and **[CMake]** installed, so, before you start, go get them!

- If you're using Apple-made operating systems, you'll want Xcode with gcc at least version 4.8.
- If you're using Windows, I highly recommend [TDM-GCC] and the IDE of your choice.
- If you're using Linux, use your package-manager to set it up for you.
  - If you use a Linux distro with yum, check [here](#fedora-core)
  - In apt-based distros, run `apt-get install build-essentials`. You'll also need autotools and automake and SVN.
- Compiler and CMake must be in your `PATH`
You will need a compiler that supports C++11

## Target layout

Bootstrapper will create an environment prepared for you to build the engine. The resulting layout when running Bootstrapper scripts is the following:

     - FUSION_ROOT/            CMake variable pointing to top-level directory do contain required libraries
       - lib/:                 Necessary libraries for the engine and also cmake modules
       - include/:             Header files for libraries and also for the engine
       - bin/:                 Tools needed for engine and game compilations
       - games/:               A convenient place to place all your games

`FUSION_ROOT` will also be used as install prefix when running make install on the engine

## Building (or downloading) your dependencies

Make sure you have CMake and your build environment ready, you are going to need them.

Create a `build` folder in the folder where this file is. Every interaction with Bootstrapper will happen inside this folder. Open a command prompt and change directory to the `buil`d folder that you just created. 

After you're inside `build`, execute these commands:

### Linux and Mac:

    cmake .. -DFUSION_ROOT=/path/to/engine/root
    make
    
### Windows:

*MinGW: *

    cmake -G "MinGW Makefiles" .. -DFUSION_ROOT=/path/to/engine/root
    mingw32-make

*MSYS (e.g, inside MSYS Bash)*

    cmake -G "MSYS Makefiles" .. -DFUSION_ROOT=/path/to/engine/root
    make
    
# Appendix

## Dependencies

Bootstrapper will build all the needed libraries, but it depends on your platform being ready to do so. This section provides a little help with some specific Linux distributions.

### Fedora Core

You will need to install a list of packages to be able to properly compile engine and its dependencies. These are the commands you will need to run (**as root**)

    yum groupinstall 'Development Tools' 'C Development Tools and Libraries'
    yum install gcc-g++ patch

[CMake]: http://cmake.org/cmake/resources/software.html
[TDM-GCC]: http://tdm-gcc.tdragon.net/
[G2CK]: http://github.com/Gear2D/g2ck
